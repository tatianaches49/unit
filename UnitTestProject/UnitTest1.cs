﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ClassLibrary;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1() { 

            // исходные данные
        int a = -353;
        bool expected = true;

        // получение значения с помощью тестируемого метода
        Number cl = new Number();
        cl.GetIntegerNum(a);
            bool actual = cl.GetIntegerNum(a);

        // сравнение ожидаемого результата с полученным
        Assert.AreEqual(expected, actual);

        }
    [TestMethod]
    public void TestMethod2()
    {
        // исходные данные
        int a = 456;
        bool expected = true;

        // получение значения с помощью тестируемого метода
        Number cl = new Number();
        cl.GetIntegerNum(a);
        bool actual = cl.GetIntegerNum(a);
        // сравнение ожидаемого результата с полученным
        Assert.AreEqual(expected, actual);

    }
    [TestMethod]
    public void TestMethod3()
    {
        // исходные данные
        double a = 0.0000002;
        bool expected = false;

        // получение значения с помощью тестируемого метода
        Number cl = new Number();
        bool actual = cl.GetIntegerNum(a);

        // сравнение ожидаемого результата с полученным
        Assert.AreEqual(expected, actual);

    }
    [TestMethod]
    public void TestMethod4()
    {
        // исходные данные
        bool a = true;
        bool expected = false;

        // получение значения с помощью тестируемого метода
        Number cl = new Number();
        bool actual = cl.GetIntegerNum(a);

        // сравнение ожидаемого результата с полученным
        Assert.AreEqual(expected, actual);

    }
    [TestMethod]
    public void TestMethod5()
    {
        // исходные данные
        string a = "word";
        bool expected = false;

        // получение значения с помощью тестируемого метода
        Number cl = new Number();
        bool actual = cl.GetIntegerNum(a);

        // сравнение ожидаемого результата с полученным
        Assert.AreEqual(expected, actual);
    }
}
}