﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Number
    {
        public bool GetIntegerNum(object a)
        {
            string k = Convert.ToString(a);
            return int.TryParse(k, out _);
        }

    }
}
